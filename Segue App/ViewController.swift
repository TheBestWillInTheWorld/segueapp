//
//  ViewController.swift
//  Segue App
//
//  Created by Will Gunby on 10/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //this @IBAction allows objects on a view controller that's the target of a segue from this view controller to use this function as an exit point
    @IBAction func unwindToParent(segue:UIStoryboardSegue){
        //get the view controller we're returning from into a variable
        var source = segue.sourceViewController  as TableViewController
        //we could access values from the other view here, but pushing data from the other view to here using the function below feels cleaner.
        //that said, my method does depend on the other view controller knowing what the type of this view controller is, so that would need considering in complex apps
    }
    
    //this function is visible on the other view in prepareForSegue
    func doSomething(obj:AnyObject){
        println(obj)
    }
    
}

