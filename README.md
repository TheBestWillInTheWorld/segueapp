Basic demo of a segue and an unwind of that segue, transferring an object back to the original view from the second view.

Attached image shows the result of control dragging a table cell to the exit of it's view (to trigger an unwind segue) once an appropriate IBAction is present in the original view controller class 

example IBAction function (the function name doesn't matter)

@IBAction func unwindToParent(segue:UIStoryboardSegue) {

}